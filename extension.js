/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const { Shell, Gio, Meta } = imports.gi;
const Main = imports.ui.main;

class Extension {
    constructor() {
    }

    enable() {
        Main.layoutManager.monitors.forEach(monitor => {
            const bg_actor = new Meta.BackgroundActor;
            const background = Main.layoutManager._backgroundGroup.get_child_at_index(
                Main.layoutManager.monitors.length - monitor.index - 1
            );

            bg_actor.set_content(background.get_content());
            
            let blur_effect = new Shell.BlurEffect({
                brightness: 0.60,
                sigma: 30
                    * monitor.geometry_scale,
                mode: Shell.BlurMode.ACTOR
            });
            
            blur_effect.scale = monitor.geometry_scale;
            
            bg_actor.add_effect(blur_effect);
            
            bg_actor.set_x(monitor.x);
            bg_actor.set_y(monitor.y);

            Main.layoutManager.overviewGroup.insert_child_at_index(
                bg_actor,
                monitor.index
            );
        });
    }

    disable() {
        Main.layoutManager.overviewGroup.get_children().forEach(actor => {
            if (actor.constructor.name === 'Meta_BackgroundActor') {
                Main.layoutManager.overviewGroup.remove_child(actor);
            }
        });
    }
}

function init() {
    return new Extension();
}
